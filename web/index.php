<?php

require_once __DIR__.'/../vendor/autoload.php';

require_once __DIR__.'/../vendor/simple-html-dom/simple-html-dom/simple_html_dom.php';

ini_set('max_execution_time', 0);
error_reporting(E_ERROR | E_PARSE);

$app = new Silex\Application();

$app['debug'] = false;

function getFollowersNumberByName($name) {
    $contextOptions = [
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false,
        ],
    ];

    $html = file_get_html(
        'https://www.instagram.com/' . $name . '/',
        false, stream_context_create($contextOptions),
        0
    );

    if ($html === false) {
        return -1;
    }

    foreach ($html->find('meta[name="description"]') as $element) {
        $meta = $element->content;

        if (strpos($meta, "Followers") > 0) {
            $followers = array_shift(explode(" ", $meta));

            return $followers * 1;
        }
    }

    return -1;
}

$app->get('/', function () use ($app) {

    $names = explode("\n", file_get_contents('tmp/people.txt'));

    $re = [];

    foreach ($names as $name) {
        $re[] = [$name, getFollowersNumberByName($name)];
    }

    $stream = function() use ($re) {
        $output = fopen('php://output', 'w');
        foreach ($re as $rec)
        {
            fputcsv($output, $rec);
        }
        fclose($output);
    };

    return $app->stream($stream, 200, array(
        'Content-Type' => 'text/csv',
        'Content-Description' => 'File Transfer',
        'Content-Disposition' => 'attachment; filename="'.urlencode("instagram-" . date("Y-m-d") . ".csv").'',
        'Expires' => '0',
        'Cache-Control' => 'must-revalidate',
        'Pragma' => 'public',
    ));
});

$app->run();